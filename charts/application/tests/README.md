# Application Helm Chart Unit Tests

## Overview

This folder contains unit tests for our Application Helm chart. These tests are designed to validate the basic behavior of the templates within the chart.

## Requirements

- Helm (v3.x)
- Helm Unit Test Plugin

## Setup

1. Install Helm v3 following the instructions from [Helm's official documentation](https://helm.sh/docs/intro/install/).

2. Install the Helm Unit Test Plugin:

    ```bash
    helm plugin install https://github.com/lrills/helm-unittest
    ```

## Running the Tests

Navigate to the root of the Helm chart directory where your `Chart.yaml` is located and run:

```bash
helm unittest .
```

# Helm Chart Testing

This will execute all test files with `_test.yaml` in the `tests/` directory of your Helm chart.

## Contributing

1. Create a new `_test.yaml` file within the `tests/` directory for your new test suite.
2. Add tests using the [syntax](https://github.com/helm-unittest/helm-unittest/blob/main/DOCUMENT.md) provided by the Helm Unit Test plugin.

