# application

![Version: 0.2.0](https://img.shields.io/badge/Version-0.2.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

Universal Helm chart developed by Applifting Cloud Engineering

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| image.repository | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.tag | string | `""` |  |
| image.shasum | string | `""` |  |
| image.overrideTag | string | `""` |  |
| imagePullSecrets | list | `[{"name":"regcred"}]` | List of secrets containing credentials to image registry. |
| nameOverride | string | `""` | String to fully override application.name template |
| common | object | `{}` | settings and values to add to all deployed objects |
| common.labels | object | `{}` | Array with labels to add to all pods |
| common.annotations | object | `{}` | Array with annotations to add to all pods |
| common.topologySpreadConstraints | list | `[]` | list with constraints controlling how pods are spread across the cluster. Ref: https://kubernetes.io/docs/concepts/scheduling-eviction/topology-spread-constraints/ |
| common.nodeSelector | object | `{}` | Array with Node labels for all pods assignment is rendered only if deployments and jobs nodeSelector is empty. ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#nodeselector |
| common.tolerations | list | `[]` | List with Tolerations for all pods assignment is rendered only if deployments and jobs tolerations is empty ref: https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/ |
| common.affinity | object | `{}` | Array with Affinity for all pods assignment is rendered only if deployments and jobs affinity is empty. ref: https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/#schedule-a-pod-using-required-node-affinity |
| common.env | object | `{}` | Array with extra environment variables to add to all pods |
| common.extraEnvConfigMaps | list | `[]` | Name of existing ConfigMap containing extra env vars for main deployment |
| common.extraEnvSecrets | list | `[]` | List of names of existing Secret containing extra env vars for all pods |
| common.podSecurityContext | object | `{}` | Set common pod's Security Context (Is rendered only if deployments and jobs podSecurityContext is empty) ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-pod |
| common.containerSecurityContext | object | `{}` | Configure Container Security Context (is rendered only if deployments and jobs containerSecurityContext is empty) ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-container |
| application.enableDeployment | bool | `true` |  |
| application.labels | object | `{}` | Array with labels to add to application deployment |
| application.annotations | object | `{}` | Array with annotations to add to application deployment |
| application.topologySpreadConstraints | list | `[]` | list with constraints controlling how pods are spread across the cluster. Ref: https://kubernetes.io/docs/concepts/scheduling-eviction/topology-spread-constraints/ |
| application.nodeSelector | object | `{}` | Array with Node labels for application pods assignment. ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#nodeselector |
| application.tolerations | list | `[]` | list with Tolerations for application pods assignment. ref: https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/ |
| application.affinity | object | `{}` | Array with Affinity for application pods assignment. ref: https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/#schedule-a-pod-using-required-node-affinity |
| application.job.migrateCommand | string | `""` | Providing enables migrating job |
| application.job.nameSuffix | string | `"migrate"` | Adds name suffix to job deployment |
| application.job.annotations | object | `{"helm.sh/hook":"post-upgrade, post-install","helm.sh/hook-delete-policy":"hook-succeeded","helm.sh/hook-weight":"0"}` | Object for configuration of helm hooks for migrating job (is configured only if application.migrateCommand is not empty) |
| application.job.restartPolicy | string | `"Never"` | Only a RestartPolicy equal to Never or OnFailure is allowed |
| application.podAnnotations | object | `{}` | Annotations for application pods. ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| application.containerPort | int | `80` | map container ports to host ports |
| application.extraContainerPorts | list | `[]` |  |
| application.replicaCount | int | `1` | Number of application replicas to deploy |
| application.autoscaling | object | `{}` | Enable HorizontalPodAutoscaler for application pods |
| application.autoscaling.enabled | bool | `false` | Whether enable horizontal pod autoscale |
| application.autoscaling.minReplicas | int | `1` | Configure a minimum amount of pods |
| application.autoscaling.maxReplicas | int | `100` | Configure a maximum amount of pods |
| application.autoscaling.targetCPUUtilizationPercentage | int | `80` | Define the CPU target to trigger the scaling actions (utilization percentage) |
| application.autoscaling.targetMemoryUtilizationPercentage | int | `80` | Define the memory target to trigger the scaling actions (utilization percentage) |
| application.revisionHistoryLimit | int | `4` | specifies how many old ReplicaSets for this Deployment you want to retain. |
| application.updateStrategy | object | `{"type":"RollingUpdate"}` | specifies the strategy used to replace old Pods by new ones. ref: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy |
| application.updateStrategy.type | string | `"RollingUpdate"` | StrategyType - Can be set to RollingUpdate or Recreate |
| application.initContainers | list | `[]` | Add additional init containers to the application pod(s). ref: https://kubernetes.io/docs/concepts/workloads/pods/init-containers/ |
| application.sidecars | list | `[]` | Add additional sidecar containers to the application pod(s) |
| application.command | list | `[]` | Override default container command (useful when using custom images) |
| application.args | list | `[]` | Override default container args (useful when using custom images) |
| application.env | object | `{}` | Array with extra environment variables to add to main deployment |
| application.extraEnvConfigMaps | list | `[]` | Name of existing ConfigMap containing extra env vars for main deployment |
| application.extraEnvSecrets | list | `[]` | Name of existing Secret containing extra env vars for main deployment |
| application.volumes | list | `[]` | Optionally specify extra list of additional volumes for the application pod(s) |
| application.volumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts for the application container(s) |
| application.startupProbe | object | `{}` | customize startupProbe on application pods. ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/#configure-probes |
| application.livenessProbe | object | `{}` | customize livenessProbe on application pods |
| application.readinessProbe | object | `{}` | customize readinessProbe on application pods |
| application.podSecurityContext | object | `{}` | Set application pod's Security Context. ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-pod |
| application.containerSecurityContext | object | `{}` | Set Configure Container Security Context. ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-container |
| application.resources | object | `{}` | We usually recommend not to specify default resources and to leave this as a conscious choice for the user. This also increases chances charts run on environments with little resources, such as Minikube. If you do want to specify resources, uncomment the following lines, adjust them as necessary, and remove the curly braces after 'resources:'. |
| cronjob.enabled | bool | `false` |  |
| cronjob.nameSuffix | string | `"cronjob"` | Adds name suffix to cronjob deployment |
| cronjob.labels | object | `{}` | Array with labels to add to cronjob deployment |
| cronjob.annotations | object | `{}` | Array with annotations to add to cronjob deployment |
| cronjob.nodeSelector | object | `{}` | Array with Node labels for cronjob pods assignment. ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#nodeselector |
| cronjob.tolerations | list | `[]` | list with Tolerations for cronjob pods assignment ref: https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/ |
| cronjob.affinity | object | `{}` | Array with Affinity for cronjob pods assignment. ref: https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/#schedule-a-pod-using-required-node-affinity |
| cronjob.schedule | string | `"0 0 * * *"` | * * * * * |
| cronjob.args | list | `[]` | Override default container args (useful when using custom images) |
| cronjob.env | object | `{}` | Array with extra environment variables to add to cronjob |
| cronjob.extraEnvConfigMaps | list | `[]` | Name of existing ConfigMap containing extra env vars for cronjob |
| cronjob.extraEnvSecrets | list | `[]` | Name of existing Secret containing extra env vars for cronjob |
| cronjob.restartPolicy | string | `"OnFailure"` | Only a RestartPolicy equal to Never or OnFailure is allowed |
| cronjob.podSecurityContext | object | `{}` | Configure cronjob's Pods Security Context. ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-pod |
| cronjob.containerSecurityContext | object | `{}` | Configure Configure Container Security Context. ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-container |
| cronjob.resources | object | `{}` | We usually recommend not to specify default resources and to leave this as a conscious choice for the user. This also increases chances charts run on environments with little resources, such as Minikube. If you do want to specify resources, uncomment the following lines, adjust them as necessary, and remove the curly braces after 'resources:'. |
| cronjob.command | list | `[]` | Override default container command (useful when using custom images) |
| worker.enabled | bool | `false` |  |
| worker.nameSuffix | string | `"worker"` | Adds name suffix to worker deployment |
| worker.imageDiff | bool | `false` | boolean specifying whether worker deployment will have a different image |
| worker.image | object | `{"overrideTag":"","repository":"","shasum":"","tag":""}` | Array for worker deployment image |
| worker.labels | object | `{}` | Array with labels to add to worker deployment |
| worker.annotations | object | `{}` | Array with annotations to add to worker deployment |
| worker.topologySpreadConstraints | list | `[]` | list with constraints controlling how pods are spread across the cluster. Ref: https://kubernetes.io/docs/concepts/scheduling-eviction/topology-spread-constraints/ |
| worker.nodeSelector | object | `{}` | Array with Node labels for worker pods assignment |
| worker.tolerations | list | `[]` | list with Tolerations for worker pods assignment |
| worker.affinity | object | `{}` | Array with Affinity for worker pods assignment |
| worker.podAnnotations | object | `{}` | Annotations for worker pods. ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| worker.replicaCount | int | `1` | Number of worker replicas to deploy |
| worker.autoscaling | object | `{}` | Enable HorizontalPodAutoscaler for worker pods |
| worker.autoscaling.enabled | bool | `false` | Whether enable horizontal pod autoscale |
| worker.autoscaling.minReplicas | int | `1` | Configure a minimum amount of pods |
| worker.autoscaling.maxReplicas | int | `100` | Configure a maximum amount of pods |
| worker.autoscaling.targetCPUUtilizationPercentage | int | `80` | Define the CPU target to trigger the scaling actions (utilization percentage) |
| worker.autoscaling.targetMemoryUtilizationPercentage | int | `80` | Define the memory target to trigger the scaling actions (utilization percentage) |
| worker.revisionHistoryLimit | int | `4` | specifies how many old ReplicaSets for this Deployment you want to retain. |
| worker.updateStrategy | object | `{"type":"RollingUpdate"}` | specifies the strategy used to replace old Pods by new ones. ref: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy |
| worker.updateStrategy.type | string | `"RollingUpdate"` | StrategyType - Can be set to RollingUpdate or Recreate |
| worker.initContainers | list | `[]` | Add additional init containers to the worker pod(s). ref: https://kubernetes.io/docs/concepts/workloads/pods/init-containers/ |
| worker.sidecars | list | `[]` | Add additional sidecar containers to the worker pod(s) |
| worker.command | list | `[]` | Override default container command (useful when using custom images) |
| worker.args | list | `[]` | Override default container args (useful when using custom images) |
| worker.env | object | `{}` | Array with extra environment variables to add to worker deployment |
| worker.extraEnvConfigMaps | list | `[]` | Name of existing ConfigMap containing extra env vars for worker deployment |
| worker.extraEnvSecrets | list | `[]` | Name of existing Secret containing extra env vars for worker deployment |
| worker.volumes | list | `[]` | Optionally specify extra list of additional volumes for the worker pod(s) |
| worker.volumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts for the worker container(s) |
| worker.startupProbe | object | `{}` | Configure extra options for worker containers' liveness and readiness probes. ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/#configure-probes |
| worker.livenessProbe | object | `{}` | customize livenessProbe on worker pods |
| worker.readinessProbe | object | `{}` | customize readinessProbe on worker pods |
| worker.podSecurityContext | object | `{}` | Configure worker's Pods Security Context. ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-pod |
| worker.containerSecurityContext | object | `{}` | Configure worker Container Security Context. ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-container |
| worker.resources | object | `{}` | We usually recommend not to specify default resources and to leave this as a conscious choice for the user. This also increases chances charts run on environments with little resources, such as Minikube. If you do want to specify resources, uncomment the following lines, adjust them as necessary, and remove the curly braces after 'resources:'. |
| externalSecrets | object | `{}` | secrets need to be also specified in `secretEnv` to be available for containers |
| externalSecrets.enabled | bool | `false` | automatic external secrets integration |
| externalSecrets.labels | object | `{}` | Array with labels to add to externalSecrets |
| externalSecrets.annotations | object | `{}` | Array with annotations to add to externalSecrets |
| externalSecrets.secretStoreName | string | `nil` | SecretStore is created by Terraform with the name `$APP_NAME-$APP_ENVIRONMENT` |
| externalSecrets.targetSecretName | string | `""` | String with name for the target secret |
| externalSecrets.creationPolicy | string | `"Owner"` | String with definition of how the operator creates the a secret |
| externalSecrets.variables | list | `[]` | list of variable names from GitLab to expose to the container |
| externalSecrets.extract | list | `[]` | list of variable names containing JSON objects that will be expanded to environment variables - each key inside the JSON object will correspond to a single environment variable |
| externalSecrets.findRegex | string | `""` | string used to find secrets based on regular expressions and rewrite the key names. |
| externalSecrets.extraDataFrom | list | `[]` | List of objects used to fetch all properties from the Provider key |
| serviceAccount.create | bool | `false` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| serviceAccount.labels | object | `{}` | Array with labels to add to serviceAccount |
| serviceAccount.annotations | object | `{}` | Array with annotations to add to serviceAccount |
| configmap.enabled | bool | `false` |  |
| configmap.name | string | `""` | String with custom name of the configmap |
| configmap.labels | object | `{}` | Array with labels to add to configmap |
| configmap.annotations | object | `{}` | Array with annotations to add to configmap |
| configmap.data | object | `{}` |  |
| service.enabled | bool | `true` |  |
| service.labels | object | `{}` | Array with labels to add to service |
| service.annotations | object | `{}` | Array with annotations to add to service |
| service.type | string | `"ClusterIP"` | String which allows you to specify what kind of Service you want |
| service.port | int | `80` | Intiger with incoming port |
| service.name | string | `"http"` | String with name of the port |
| ingress.enabled | bool | `false` |  |
| ingress.className | string | `""` |  |
| ingress.labels | object | `{}` | Array with labels to add to ingresss |
| ingress.annotations | object | `{}` | Array with annotations to add to ingresss |
| ingress.hosts[0].host | string | `"chart-example.local"` |  |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| ingress.tls | list | `[]` |  |
| extraObjects | list | `[]` | Extra objects to deploy (value evaluated as a template) |
| serviceMonitor.enabled | bool | `false` |  |
| serviceMonitor.labels | object | `{}` | Array with labels to add to serviceMonitor |
| serviceMonitor.interval | string | `"30s"` | How often should the metrics be scraped |
| serviceMonitor.path | string | `"/metrics"` | HTTP path to scrape for metrics. |
| serviceMonitor.scheme | string | `""` | HTTP scheme to use for scraping. |
| serviceMonitor.tlsConfig | object | `{}` | TLS configuration to use when scraping the endpoint |
| serviceMonitor.scrapeTimeout | string | `""` | Timeout after which the scrape is ended |

